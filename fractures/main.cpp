/*
 * Netgen mesh generator
 */
#include <fstream>
#include <sstream>
#include <iostream>
#include <vector>
#include <cmath>
#include <ctime>

#define PI 3.14159265

void crossingFracs(int& fcount, int fracbox[], std::ostringstream fracStr[], std::ostringstream& gfracStr, std::ostringstream& planeStr, int& count3, int c, int hmax) {

    int count = fcount;
    fcount += 2;

    for(int i = fracbox[count]; i < fracbox[count + 1]; i++) {
            int m = count/2;
            fracStr[m] << " and frac" << i;

            if (fracbox[count + 2] == 0) {
                for (int j = 0; j <= m; j++) {
                    gfracStr << fracStr[j].str();
                }

                planeStr << "solid mygeom" << count3 << " = cube" << c << gfracStr.str() << " -maxh = " << hmax << "; \n";
                planeStr << "tlo mygeom" << count3 << " -transparent; \n\n";
                count3++;

                gfracStr.str("");
                gfracStr.clear();
            } else {
                crossingFracs(fcount, fracbox, fracStr, gfracStr, planeStr, count3, c, hmax);
            }

            fracStr[m].str("");
            fracStr[m].clear();
    }
    fcount -= 2;
}

void generateFracturedDomain3D() {
    std::string meshFineFile1 = "mesh.geo";
    std::ofstream file1;
    file1.open(meshFineFile1.data());

    std::string meshFineFile2 = "frac1.txt";
    // std::ifstream file2;
    std::ofstream file2;
    file2.open(meshFineFile2.data());

    file1 << "# fractured domain \n";
    file1 << "algebraic3d \n\n";

    // количество трещин
    int Nf = 10;
    
    // границы области 
    int v0 = 0, vn = 1000;

    // количество грубых блоков на каждую ось 
    int n = 5;

    // параметр   
    int dn = 10;
    
    // метка трещин
    int bc = 3;
    
    double Lx = vn-v0;
    double Ly = vn-v0;
    double Lz = vn-v0;
    
    int Nx = n;
    int Ny = n;
    int Nz = n;
    double hx = Lx/Nx;
    double hy = Ly/Ny;
    double hz = Lz/Nz;
    
    double hmax = Lx/Nx/dn;

    std::vector<double> fractures;
    std::ostringstream fracs;
    int num = 1, count1 = 1;
    
    int v1 = 10, v2 = vn-2*v1;
    int a1 = -90, a2 = -a1*2;
    double b1 =-(100+a1)/100., b2 = -b1;

    double _x[Nf], _y[Nf], _z[Nf], _nx[Nf], _ny[Nf], _nz[Nf];
    double fnx[3], fny[3], fnz[3];

    fnx[0] =-1; fnx[1] = 0; fnx[2] = 1;
    fny[0] = 0; fny[1] =-1; fny[2] = 1;
    fnz[0] = 1; fnz[1] =-1; fnz[2] = 0;

    srand(time(NULL));
    for (int f = 0; f < Nf; f++) {

        // _x[f] = (v1 + rand() % v2), _y[f] = (v1 + rand() % v2), _z[f] = (v1 + rand() % v2);
        // _nx[f] = (a1 + rand() % a2)/100., _ny[f] = (a1 + rand() % a2)/100., _nz[f] = (a1 + rand() % a2)/100.;

        // file2 >> _x[f]; 
        // file2 >> _y[f]; 
        // file2 >> _z[f]; 
        // file2 >> _nx[f]; 
        // file2 >> _ny[f]; 
        // file2 >> _nz[f];
    
        while(true) {

            _x[f] = (v1 + rand() % v2), _y[f] = (v1 + rand() % v2), _z[f] = (v1 + rand() % v2);
            _nx[f] = (a1 + rand() % a2)/100., _ny[f] = (a1 + rand() % a2)/100., _nz[f] = (a1 + rand() % a2)/100.;

            while(true) {
                _nx[f] = (a1 + rand() % a2)/100.;
                if (!((_nx[f] > b1)&&(_nx[f] < b2))) break;
            }
            while(true) {
                _ny[f] = (a1 + rand() % a2)/100.;
                if (!((_ny[f] > b1)&&(_ny[f] < b2))) break;
            }
            while(true) {
                _nz[f] = (a1 + rand() % a2)/100.;
                if (!((_nz[f] > b1)&&(_nz[f] < b2))) break;
            }

            int f1 = 0;
            for (int i = 0; i < 3; i++) {
                double val1 = fabs(_nx[f]*fnx[i]+_ny[f]*fny[i]+_nz[f]*fnz[i]);
                double val21 = sqrt(_nx[f]*_nx[f] + _ny[f]*_ny[f] + _nz[f]*_nz[f]);
                double val22 = sqrt(fnx[i]*fnx[i] + fny[i]*fny[i] + fnz[i]*fnz[i]);
                double aRad = acos(val1/val21/val22);
                double aDegree = aRad*180.0/PI;

                if (aDegree < 25.0) f1++;
            }

            for (int i = 0; i < f; i++) {
                double val1 = fabs(_nx[f]*_nx[i]+_ny[f]*_ny[i]+_nz[f]*_nz[i]);
                double val21 = sqrt(_nx[f]*_nx[f] + _ny[f]*_ny[f] + _nz[f]*_nz[f]);
                double val22 = sqrt(_nx[i]*_nx[i] + _ny[i]*_ny[i] + _nz[i]*_nz[i]);
                double aRad = acos(val1/val21/val22);
                double aDegree = aRad*180.0/PI;

                if (aDegree < 25.0) f1++;

                // printf("f %i, i %i, radian %g, degree %g \n", f, i, aRad, aDegree);
            }

            if (f1 == 0) break;
        } 
        
        file2 << _x[f] << " " << _y[f] << " " << _z[f] << " " << _nx[f] << " " << _ny[f] << " " << _nz[f] << "\n";
        
        fractures.push_back(_x[f]);
        fractures.push_back(_y[f]);
        fractures.push_back(_z[f]);
        fractures.push_back(_nx[f]);
        fractures.push_back(_ny[f]);
        fractures.push_back(_nz[f]);

        for(int i = num; i < num+2; i++) {
            if (i == num) {
                fracs << "solid frac" << count1 << " = plane (" << _x[f] << ", " << _y[f] << ", " << _z[f] << "; " << _nx[f] << ", " << _ny[f] << ", " << _nz[f] << ") -bc="<< bc <<"; \n";
                count1++;
            } else {
                fracs << "solid frac" << count1 << " = plane (" << _x[f] << ", " << _y[f] << ", " << _z[f] << "; " << -_nx[f] << ", " << -_ny[f] << ", " << -_nz[f] << ") -bc="<< bc <<"; \n";
                count1++;
            }
        }
        num += 2; 
    }

    int N = Nx*Ny*Nz;
    std::ostringstream planeStr;
    int ind = 1; int count3 = 1;
    int counts[N];
    for(int i = 0; i < Nx; i++){   
        double x1 = i*hx;
        double x2 = (i+1)*hx;
        for(int j = 0; j < Ny; j++){   
            double y1 = j*hy;
            double y2 = (j+1)*hy;
            for(int k = 0; k < Nz; k++){   
                double z1 = k*hz;
                double z2 = (k+1)*hz;
                
                file1 << "solid cplane" << ind << " = plane (" << x1 << ", " << y1 << ", " << z1 << "; " 
                                                              << 0 << ", " << 0 << ", " << -1 << "); \n"; 
                file1 << "solid cplane" << ind+1 << " = plane (" << x1 << ", " << y1 << ", " << z1 << "; " 
                                                              << 0 << ", " << -1 << ", " << 0 << "); \n"; 
                file1 << "solid cplane" << ind+2 << " = plane (" << x1 << ", " << y1 << ", " << z1 << "; " 
                                                              << -1 << ", " << 0 << ", " << 0 << "); \n"; 
                file1 << "solid cplane" << ind+3 << " = plane (" << x2 << ", " << y2 << ", " << z2 << "; " 
                                                              << 0 << ", " << 0 << ", " << 1 << "); \n";       
                file1 << "solid cplane" << ind+4 << " = plane (" << x2 << ", " << y2 << ", " << z2 << "; " 
                                                              << 0 << ", " << 1 << ", " << 0 << "); \n"; 
                file1 << "solid cplane" << ind+5 << " = plane (" << x2 << ", " << y2 << ", " << z2 << "; " 
                                                              << 1 << ", " << 0 << ", " << 0 << "); \n";

                file1 << "solid plane" << ind << " = plane (" << x1 << ", " << y1 << ", " << z1 << "; " 
                                                              << -1 << ", " << 0 << ", " << 1 << "); \n"; 

                file1 << "solid plane" << ind+1 << " = plane (" << x1 << ", " << y1 << ", " << z1 << "; " 
                                                              << 1 << ", " << 0 << ", " << -1 << "); \n";

                file1 << "solid plane" << ind+2 << " = plane (" << x2 << ", " << y1 << ", " << z1 << "; " 
                                                              << 0 << ", " << -1 << ", " << 1 << "); \n";

                file1 << "solid plane" << ind+3 << " = plane (" << x2 << ", " << y1 << ", " << z1 << "; " 
                                                              << 0 << ", " << 1 << ", " << -1 << "); \n";

                file1 << "solid plane" << ind+4 << " = plane (" << x1 << ", " << y1 << ", " << z1 << "; " 
                                                              << 1 << ", " << -1 << ", " << 0 << "); \n"; 

                file1 << "solid plane" << ind+5 << " = plane (" << x1 << ", " << y1 << ", " << z1 << "; " 
                                                              << -1 << ", " << 1 << ", " << 0 << "); \n";

                file1 << "solid cube" << ind << " = cplane" << ind << " and cplane" << ind+5 
                            << " and plane" << ind+2 << " and plane" << ind+5 << "; \n"; 

                file1 << "solid cube" << ind+1 << " = cplane" << ind+1 << " and cplane" << ind+5 
                            << " and plane" << ind << " and plane" << ind+3 << "; \n";

                file1 << "solid cube" << ind+2 << " = cplane" << ind+1 << " and cplane" << ind+3 
                            << " and plane" << ind+1 << " and plane" << ind+5 << "; \n";

                file1 << "solid cube" << ind+3 << " = cplane" << ind << " and cplane" << ind+4 
                            << " and plane" << ind << " and plane" << ind+4 << "; \n";

                file1 << "solid cube" << ind+4 << " = cplane" << ind+2 << " and cplane" << ind+3 
                            << " and plane" << ind+3 << " and plane" << ind+4 << "; \n"; 

                file1 << "solid cube" << ind+5 << " = cplane" << ind+2 << " and cplane" << ind+4 
                            << " and plane" << ind+1 << " and plane" << ind+2 << "; \n";  

                double tps[36]; int t = 0;
                tps[t] = x2; t++; tps[t] = y1; t++; tps[t] = z1; t++; tps[t] = x2; t++; tps[t] = y2; t++; tps[t] = z1; t++;  
                tps[t] = x2; t++; tps[t] = y1; t++; tps[t] = z1; t++; tps[t] = x2; t++; tps[t] = y1; t++; tps[t] = z2; t++;  
                tps[t] = x1; t++; tps[t] = y1; t++; tps[t] = z2; t++; tps[t] = x2; t++; tps[t] = y1; t++; tps[t] = z2; t++; 
                tps[t] = x1; t++; tps[t] = y2; t++; tps[t] = z1; t++; tps[t] = x2; t++; tps[t] = y2; t++; tps[t] = z1; t++;  
                tps[t] = x1; t++; tps[t] = y1; t++; tps[t] = z2; t++; tps[t] = x1; t++; tps[t] = y2; t++; tps[t] = z2; t++; 
                tps[t] = x1; t++; tps[t] = y2; t++; tps[t] = z1; t++; tps[t] = x1; t++; tps[t] = y2; t++; tps[t] = z2; t = 0;

                for (int c = ind; c < ind+6; c++) {           
                    int num = 1, count = 0;
                    int fracbox[Nf*2+1];
                    for (int f = 0 ; f < Nf*2+1; f++) {
                        fracbox[f] = 0;
                    }

                    for (int f = 0 ; f < Nf; f++) {
                        double x = fractures[f*6], y = fractures[f*6+1], z = fractures[f*6+2];
                        double nx = fractures[f*6+3], ny = fractures[f*6+4], nz = fractures[f*6+5];

                        double A = nx, B = ny, C = nz, D = -nx*x-ny*y-nz*z; 
                        double eps = 0;
                        
                        if (!(((A*x1+B*y1+C*z1+D>-eps)&&(A*tps[t]+B*tps[t+1]+C*tps[t+2]+D>-eps)
                        &&(A*tps[t+3]+B*tps[t+4]+C*tps[t+5]+D>-eps)&&(A*x2+B*y2+C*z2+D>-eps))
                        ||((A*x1+B*y1+C*z1+D< eps)&&(A*tps[t]+B*tps[t+1]+C*tps[t+2]+D< eps)
                        &&(A*tps[t+3]+B*tps[t+4]+C*tps[t+5]+D< eps)&&(A*x2+B*y2+C*z2+D< eps)))) {
                            fracbox[count] = num;
                            count++;
                            fracbox[count] = num+2;
                            count++;
                        }
                        num += 2;
                    }

                    if (count > 0) {
                        std::ostringstream gfracStr;
                        std::ostringstream fracStr[Nf];
                        int fcount = 0;
                        crossingFracs(fcount, fracbox, fracStr, gfracStr, planeStr, count3, c, hmax);
                    } else {
                        planeStr << "solid mygeom" << count3 << " = cube" << c << " -maxh = " << hmax << "; \n";
                        planeStr << "tlo mygeom" << count3 << " -transparent; \n\n";
                        count3++;
                    }
                    t += 6;
                }
                ind += 6;
            }
        }
    }  
    file1 << fracs.str();
    file1 << "\n";
    file1 << planeStr.str();
    file1 << "\n";

    file1.close();
    file2.close();
}

int main(int argc, char** args) {

    generateFracturedDomain3D();

    return 0;
}
