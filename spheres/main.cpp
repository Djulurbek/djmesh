/*
 * Netgen mesh generator
 */
#include <fstream>
#include <sstream>
#include <iostream>
#include <vector>
#include <cmath>
#include <ctime>

void generatePerforatedDomain3D() {
    std::string meshFineFile1 = "mesh.geo";
    std::ofstream file1;
    file1.open(meshFineFile1.data());

    std::string meshFineFile2 = "spheres.txt";
    std::ofstream file2;
    file2.open(meshFineFile2.data());

    file1 << "# spheres domain \n";
    file1 << "algebraic3d \n\n";
    
    int v0 = 0, vn = 1000;
    int n = 5, dn = 10, Nc = 200;
    int bc = 3;    
    
    double Lx = vn-v0;
    double Ly = vn-v0;
    double Lz = vn-v0;
    
    int Nx = n;
    int Ny = n;
    int Nz = n;
    double hx = Lx/Nx;
    double hy = Ly/Ny;
    double hz = Lz/Nz;
    double hmax = Lx/Nx/dn;

    srand(time(NULL));

    int r1 = 2000, r2 = 3000, v1 = r2/100+10, v2 = 1000-v1*2;
    double cx[Nc], cy[Nc], cz[Nc], cr[Nc];
    int count1 = 1;
    for(int i = 0; i < Nc; i++) {
        while(true) 
        {
            cx[i] = (v1 + rand() % v2), cy[i] = (v1 + rand() % v2), cz[i] = (v1 + rand() % v2);
            cr[i] = (r1 + rand() % (r2-r1))/100.;

            int f1 = 0;
            for (int j = 0; j < i; j++) {
                double l = std::sqrt( std::pow(cx[i] - cx[j],2)+std::pow(cy[i] - cy[j],2)+std::pow(cz[i] - cz[j],2));

                if (l - cr[i] - cr[j] < 5) f1++;
            }

            if (f1 == 0) break;
        }

        file2 << cx[i] << " " << cy[i] << " " << cz[i] << " " << cr[i] << "\n";

        file1 << "solid sphere" << count1 << " = sphere (" << cx[i] << ", " << cy[i] << ", " << cz[i] << "; " << cr[i] << ") -bc="<< bc <<"; \n";

        count1++;
    }

    std::ostringstream inclStr1;
    int N = Nx*Ny*Nz;
    int ind = 1; 
    for(int i = 0; i < Nx; i++) {   
        double x1 = i*hx;
        double x2 = (i+1)*hx;
        for(int j = 0; j < Ny; j++){   
            double y1 = j*hy;
            double y2 = (j+1)*hy;
            for(int k = 0; k < Nz; k++){   
                double z1 = k*hz;
                double z2 = (k+1)*hz;

                file1 << "solid cube" << ind << " = orthobrick (" << x1 << ", " << y1 << ", " << z1 << "; "<< x2 << ", " << y2 << ", " << z2 << "); \n";
                    
                std::ostringstream inclStr2;    
                for(int c = 0; c < Nc; c++) {
                    double lx = std::abs((x1+x2)/2 - cx[c]);
                    double ly = std::abs((y1+y2)/2 - cy[c]);
                    double lz = std::abs((z1+z2)/2 - cz[c]);
                    if ((lx <= hx/2+cr[c])&&(ly <= hy/2+cr[c])&&(lz <= hz/2+cr[c])) {
                        inclStr2 << " and not sphere" << c+1;       
                    } 
                }

                inclStr1 << "solid mygeom" << ind << " = cube" << ind << inclStr2.str() << " -maxh = " << hmax << "; \n";
                inclStr1 << "tlo mygeom" << ind << "; \n\n";

                ind++;
            }
        }
    }  

    file1 << inclStr1.str();
    file1.close();
    file2.close();
}

int main(int argc, char** args) {

    generatePerforatedDomain3D();
            
    return 0;
}
